export const state = () => ({
  user: {},
  userInfo: [],
  token: null
});

export const mutations = {
  setUser(state, payload) {
    state.user = payload
  },
  setToken(state, payload) {
    state.token = payload
  },
  setUserInfo(state, payload) {
    state.userInfo = payload
  }
}
export const getters = {
  isLoggedIn: (state) => state.isLoggedIn,
  errors: (state) => state.errors,
};

export const actions = {
  async login({commit}, {email, password}) {
    try {
      commit('setUser', {email, password})
      let data = await this.$axios.$post('https://api.quwi.com/v2/auth/login', {email, password})
      commit('setToken', data.token)
      commit('setUserInfo', data)
      return data
    } catch (e) {
      console.log(e, 'ERROR')
    }
  },
}
